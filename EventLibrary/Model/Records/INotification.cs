﻿using System;

namespace EventLibrary.Model.Records
{
    public interface INotification
    {
        event Action Notify;
        DateTime NotifyDate { get; set; }
    }
}
