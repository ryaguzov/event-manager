﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EventLibrary.TextEditor
{
    /// <summary>
    /// Логика взаимодействия для TextEditor.xaml
    /// </summary>
            
    public partial class TextEditor : UserControl
    {
        public TextEditor()
        {
            InitializeComponent();

        }
  /*
        public static readonly DependencyProperty TextControlProperty =
            DependencyProperty.Register("TextControl", typeof(TextRange), typeof(TextEditor), new PropertyMetadata(OnSetTextChanged));
      
        public TextRange TextControl
        {
            get { return (TextRange)GetValue(TextControlProperty); }
            set { SetValue(TextControlProperty, value); }
        }

        private static void OnSetTextChanged(DependencyObject d,
         DependencyPropertyChangedEventArgs e)
        {
            TextEditor UserControl1Control = d as TextEditor;
            UserControl1Control.SetTextRange(e);
        }

        private void SetTextRange(DependencyPropertyChangedEventArgs e)
        {
            textRange = new TextRange(Doc.ContentStart, Doc.ContentEnd);
        }
        */

        public static readonly DependencyProperty SetTextProperty =
        DependencyProperty.Register("SetText", typeof(string), typeof(TextEditor));

        public string SetText
        {
            get
            {
                return (string)GetValue(SetTextProperty);
            }
            set
            {
                SetValue(SetTextProperty, value);
            }
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            if(e.Property == SetTextProperty)
            {
                SetText = e.NewValue.ToString();
                TestTextBlock.Text = e.NewValue.ToString();
            }
        }
        
        private void TestTextBlock_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox temp = sender as TextBox;
            SetText = temp.Text;
        }
    }
}
