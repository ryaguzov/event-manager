﻿using System;

namespace EventLibrary.Model.Records
{
    public class Notification : BaseRecord, INotification
    {
        public DateTime NotifyDate { get; set; }
        public event Action Notify;
        
        //private
    }
}
