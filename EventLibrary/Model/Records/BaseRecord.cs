﻿using System;

namespace EventLibrary.Model.Records
{
    [Serializable]
    public class BaseRecord : IBaseRecord
    {
        public string Name { get; set; }
        public string Сontent { get; set; }
        protected DateTime _created;
    }
}
