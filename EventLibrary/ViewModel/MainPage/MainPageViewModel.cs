﻿using EventLibrary.Model;
using EventLibrary.Model.Records;
using StkCommon.UI.Wpf.Commands;
using StkCommon.UI.Wpf.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventLibrary.ViewModel.MainPage
{
    public class MainPageViewModel : ViewModelBase
    {
        private DesignEventManager _designEventManager;

        public DesignEvent SelectedEvent { get; set; }
        public Notification SelectedNotification { get; set; }

        public ObservableCollection<DesignEvent> Events { get; private set; }
        public ObservableCollection<Notification> Notifications { get; private set; }

        private Note lastNote;
        public Note LastNote
        {
            get
            {
                if (lastNote != (_designEventManager.Notes.Count > 0 ? _designEventManager.Notes.Last() : null))
                {
                    lastNote = _designEventManager.Notes.Count > 0 ? _designEventManager.Notes.Last() : null;
                    OnPropertyChanged();
                }
                return lastNote;
            }
        }

        public DelegateCommand<DesignEvent> RemoveEventCommand =>
            new DelegateCommand<DesignEvent>((item) =>
            {
                _designEventManager.RemoveEvent(item);
            }, (item) => (item) != null);

        public DelegateCommand<Notification> RemoveNotificationCommand =>
            new DelegateCommand<Notification>((item) =>
            {
                _designEventManager.RemoveNotification(item);
            }, (item) => (item) != null);

        public DelegateCommand<Note> RemoveNoteCommand =>
            new DelegateCommand<Note>((item) =>
            {
                _designEventManager.RemoveNote(item);
            }, (item) => (item) != null);


        public MainPageViewModel( DesignEventManager designEventManager)
        {
            _designEventManager = designEventManager;

            //lastNote = 

            Events = _designEventManager.CurrentEvents;
            Notifications = _designEventManager.CurrentNotifications;
        }
    }
}
