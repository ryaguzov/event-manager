﻿using EventLibrary.Model.Records;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace EventLibrary.Model
{
    public class RecordLoader<T> where T : BaseRecord
    {
        private string _path;
        private FileStream _file;
        private BinaryFormatter _serializer = new BinaryFormatter();

        public RecordLoader( string path )
        {
            _path = path;            
        }

        public void Load(out List<T> list)
        {
            if (File.Exists(_path) && new FileInfo(_path).Length != 0)
            {
                _file = new FileStream(_path, FileMode.Open, FileAccess.Read);
                list = _serializer.Deserialize(_file) as List<T>;                
            }
            else
            {
                _file = new FileStream(_path, FileMode.Create);
                list = null;
            }
            _file.Close();
        }

        public void Save(List<T> list)
        {
            _file = new FileStream(_path, FileMode.OpenOrCreate);
            _serializer.Serialize(_file, list);
            _file.Close();
        }
    }
}
