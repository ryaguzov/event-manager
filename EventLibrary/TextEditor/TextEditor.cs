﻿using StkCommon.UI.Wpf.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace EventLibrary.ViewModel.Create
{
    public class TextEditor : TextBox
    {
        public string TextContent { get; set; }
        public TextEditor() { }
    }
}
