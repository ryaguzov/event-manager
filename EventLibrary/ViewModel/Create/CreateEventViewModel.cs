﻿using EventLibrary.Model;
using StkCommon.UI.Wpf.ViewModels;
using System;
using StkCommon.UI.Wpf.Commands;
using EventLibrary.Model.Records;

namespace EventLibrary.ViewModel.Create
{
    public class CreateEventViewModel : CreateFormBaseViewModel
    {
        private DateTime startDate = DateTime.Now;
        public DateTime StartDateField
        {
            get => startDate;
            set
            {
                startDate = value;
                OnPropertyChanged("StartDate");
            }
        }
        public DateTime EndDateField { get; set; } = DateTime.Now;
        public DateTime NotifyDateField { get; set; } = DateTime.Now;

        public DateTime StartTimeField { get; set; }
        public DateTime EndTimeField { get; set; }

        public int PriorityField { get; set; }
        public bool IsToday { get; set; } = true;

        public override DelegateCommand CreateCommand { get; }

        public CreateEventViewModel( DesignEventManager designEventManger ) : base(designEventManger)
        {
            CreateCommand = new DelegateCommand(_ => AddEvent());
            
            PropertyChanged += (ob, arg) =>
            {
                if (string.Equals(arg.PropertyName, "StartDate"))
                    IsToday = startDate.Date == DateTime.Now.Date ? true : false;
            };
        }

        private void AddEvent()
        {
            if (string.IsNullOrEmpty(NameField))
            {
                NameField = "(без названия)";
            }

            var Event = new DesignEvent() { Name = NameField,
                                            Сontent = ContentField,
                                            StartDate = new DateTime(StartDateField.Year, StartDateField.Month, StartDateField.Day, StartTimeField.Hour, StartTimeField.Minute, 0),
                                            EndDate = new DateTime(EndDateField.Year, EndDateField.Month, EndDateField.Day, EndTimeField.Hour, EndTimeField.Minute, 0),
                                            Priority = PriorityField,
                                            NotifyDate = NotifyDateField};

            _designEventManger.AddEvent( Event, IsToday );

            ClearFields();

            СreationFinished();
        }

        private void ClearFields()
        {
            NameField = string.Empty;
            ContentField = string.Empty;
            StartDateField = DateTime.Now;
            EndDateField = DateTime.Now;
            NotifyDateField = DateTime.Now;
            PriorityField = 0;

            OnPropertyChanged();
        }
    }
}
