﻿using EventLibrary.Model.Records;
using EventLibrary.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventLibrary.Model
{
    public class DesignEventManager
    {
        public ObservableCollection<DesignEvent> Events { get; private set;}
        public ObservableCollection<Note> Notes { get; private set; }
        public ObservableCollection<Notification> Notifications { get; private set; }

        public ObservableCollection<DesignEvent> CurrentEvents { get; private set; }

        public ObservableCollection<Notification> CurrentNotifications { get; private set; }

        private RecordLoader<DesignEvent> eventRecordLoader = new RecordLoader<DesignEvent>("events.ev1");
        private RecordLoader<Note> noteRecordLoader = new RecordLoader<Note>("notes.ed");
        private RecordLoader<Notification> notificationRecordLoader = new RecordLoader<Notification>("notifications.ed");

        public DesignEventManager()
        {            
            var events = new List<DesignEvent>();
            eventRecordLoader.Load(out events);

            var notes = new List<Note>();
            noteRecordLoader.Load(out notes);

            Events = events == null ? new ObservableCollection<DesignEvent>() : new ObservableCollection<DesignEvent>(events);
            CurrentEvents = events == null ? new ObservableCollection<DesignEvent>() : new ObservableCollection<DesignEvent>(events);

            Notes = notes == null ? new ObservableCollection<Note>() : new ObservableCollection<Note>(notes);

            /*
            Notifications = new ObservableCollection<Notification>();
            CurrentNotifications = new ObservableCollection<Notification>();
            */
        }

        public void AddEvent(DesignEvent item, bool isToday = false)
        {
            Events.Add(item);
            if (isToday)
                CurrentEvents.Add(item);
            eventRecordLoader.Save(Events.ToList());
        }

        public void AddNote(Note item)
        {
            Notes.Add(item);
            noteRecordLoader.Save(Notes.ToList());
        }

        public void AddNotification(Notification item)
        {
            Notifications.Add(item);
            //noteRecordLoader.Save(Events.ToList());
        }

        public void RemoveEvent(DesignEvent item)
        {
            CurrentEvents.Remove(item);
            Events.Remove(item);
            eventRecordLoader.Save(Events.ToList());
        }

        public void RemoveNote(Note item)
        {
            Notes.Remove(item);
            noteRecordLoader.Save(Notes.ToList());
        }

        public void RemoveNotification(Notification item)
        {
            Notifications.Remove(item);
            //recordLoader.Save(Events.ToList());
        }

        public void RemoveCurrentEvent(DesignEvent item, bool completely = false)
        {
            CurrentEvents.Remove(item);
            if (completely)
                Events.Remove(item);
            eventRecordLoader.Save(Events.ToList());
        }

        public void RemoveCurrentNote(Note item, bool completely = false)
        {
            if (completely)
            {
                Notes.Remove(item);
                //LastNote = Notes.Last();
            }
            else
            {
                //LastNote = Notes[Notes.Count-2];
            }
            noteRecordLoader.Save(Notes.ToList());
        }

        public void RemoveCurrentNotification(Notification item, bool completely = false)
        {
            CurrentNotifications.Remove(item);
            if (completely)
                Notifications.Remove(item);
            //noteRecordLoader.Save(Notes.ToList());
        }
    }
}
