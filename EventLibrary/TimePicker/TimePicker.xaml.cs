﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EventLibrary.TimePicker
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class TimePicker : UserControl
    {
        public TimePicker()
        {
            InitializeComponent();
        }


        public int Hours
        {
            get { return (int)GetValue(HoursProperty); }
            set { SetValue(HoursProperty, value); }
        }

        public static readonly DependencyProperty HoursProperty =
            DependencyProperty.Register("Hours", typeof(int), typeof(TimePicker), new PropertyMetadata(DateTime.Now.Hour, HoursChanged));

        private static void HoursChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var temp = (TimePicker)d;
            var min = temp.Time.Minute;
            var hours = (int)(e.NewValue);

            temp.Time = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, hours, min, 0);
        }

        public int Minutes
        {
            get { return (int)GetValue(MinutesProperty); }
            set { SetValue(MinutesProperty, value); }
        }

        public static readonly DependencyProperty MinutesProperty =
            DependencyProperty.Register("Minutes", typeof(int), typeof(TimePicker), new PropertyMetadata(DateTime.Now.Minute, MinutesChanged));

        private static void MinutesChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var temp = (TimePicker)d;
            var min = (int)(e.NewValue);
            var hours = temp.Time.Hour;

            temp.Time = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, hours, min, 0);
        }

        public Brush BackgroundColor
        {
            get { return (Brush)GetValue(BackgroundColorProperty); }
            set { SetValue(BackgroundColorProperty, value); }
        }

        public static readonly DependencyProperty BackgroundColorProperty =
            DependencyProperty.Register("BackgroundColor", typeof(Brush), typeof(TimePicker), new PropertyMetadata(null));


        public Brush TextBackgroundColor
        {
            get { return (Brush)GetValue(TextBackgroundColorProperty); }
            set { SetValue(TextBackgroundColorProperty, value); }
        }

        public static readonly DependencyProperty TextBackgroundColorProperty =
            DependencyProperty.Register("TextBackgroundColor", typeof(Brush), typeof(TimePicker), new PropertyMetadata(null));




        public Brush TextColor
        {
            get { return (Brush)GetValue(TextColorProperty); }
            set { SetValue(TextColorProperty, value); }
        }

        public static readonly DependencyProperty TextColorProperty =
            DependencyProperty.Register("TextColor", typeof(Brush), typeof(TimePicker), new PropertyMetadata(Brushes.Black));




        public DateTime Time
        {
            get { return (DateTime)GetValue(TimeProperty); }
            set { SetValue(TimeProperty, value); }
        }

        public static readonly DependencyProperty TimeProperty =
            DependencyProperty.Register("Time", typeof(DateTime), typeof(TimePicker), new PropertyMetadata(DateTime.Now));

        
        private void TextBox_GotMouseCapture(object sender, MouseEventArgs e)
        {
           var text = sender as TextBox;
            text?.SelectAll();
        }
    }
}
