﻿using EventLibrary.Model.Records;
using System;

namespace EventLibrary.Model.Records
{
    [Serializable]
    public class DesignEvent : BaseRecord, INotification
    {
        public string StartDateString { get => StartDate.ToString("dd MMMM yy"); }
        public string StartTimeString { get => StartDate.ToString("HH:mm"); }
        
        public DateTime StartDate { get; set; } = DateTime.Now;

        public string EndDateString { get => EndDate.ToString("dd MMMM yy"); }
        public string EndTimeString { get => EndDate.ToString("HH:mm"); }

        public DateTime EndDate { get; set; } = DateTime.Now;

        public int Priority { get; set; }

        public event Action Notify;
        public DateTime NotifyDate { get; set; } = DateTime.Now;


    }
}
