﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace EventLibrary.TextBoxExt
{
    /// <summary>
    /// Логика взаимодействия для TextBoxExt.xaml
    /// </summary>
    public partial class TextBoxExt : UserControl
    {
        public TextBoxExt()
        {
            InitializeComponent();
        }

        private bool Focused;

        public double TextOpacity
        {
            get { return (double)GetValue(TextOpacityProperty); }
            set { SetValue(TextOpacityProperty, value); }
        }
        private static readonly DependencyProperty TextOpacityProperty =
            DependencyProperty.Register("TextOpacity", typeof(double), typeof(TextBoxExt), new PropertyMetadata(0.5));

        public bool AcceptsReturn
        {
            get { return (bool)GetValue(AcceptsReturnProperty); }
            set { SetValue(AcceptsReturnProperty, value); }
        }       
        public static readonly DependencyProperty AcceptsReturnProperty =
            DependencyProperty.Register("AcceptsReturn", typeof(bool), typeof(TextBoxExt), new PropertyMetadata(false));

        public bool AcceptsTab
        {
            get { return (bool)GetValue(AcceptsTabProperty); }
            set { SetValue(AcceptsTabProperty, value); }
        }
        public static readonly DependencyProperty AcceptsTabProperty =
            DependencyProperty.Register("AcceptsTab", typeof(bool), typeof(TextBoxExt), new PropertyMetadata(false));


        public TextWrapping TextWrapping
        {
            get { return (TextWrapping)GetValue(TextWrappingProperty); }
            set { SetValue(TextWrappingProperty, value); }
        }
        public static readonly DependencyProperty TextWrappingProperty =
            DependencyProperty.Register("TextWrapping", typeof(TextWrapping), typeof(TextBoxExt), new PropertyMetadata( TextWrapping.NoWrap ));



        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string),
                                           typeof(TextBoxExt), new PropertyMetadata(TextSeted));

        private static void TextSeted(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var temp = (TextBoxExt)d;
            if (string.IsNullOrEmpty(e.NewValue?.ToString()) && !temp.Focused)
            {
                temp.Text = temp.backText;
                temp.TextOpacity = 0.4;
            } 
        }

        private string backText;
        public string BackText
        {
            get { return (string)GetValue(BackTextProperty); }
            set { SetValue(BackTextProperty, value); }
        }

        public static readonly DependencyProperty BackTextProperty =
            DependencyProperty.Register("BackText", typeof(string),
                                           typeof(TextBoxExt), new FrameworkPropertyMetadata(BackTextSeted));

        private static void BackTextSeted(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var temp =((TextBoxExt)d);
            temp.Text = e.NewValue.ToString();
            temp.backText = e.NewValue.ToString();
        }
        
        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            Focused = false;

            if (string.Compare(Text, string.Empty) != 0) return;
            Text = backText;
            TextOpacity = 0.4;
        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            Focused = true;

            if (string.Compare(Text, backText) != 0) return;
            Text = string.Empty;
            TextOpacity = 1;
        }
    }
}
