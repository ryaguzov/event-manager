﻿using System;

namespace EventLibrary.Model.Records
{
    [Serializable]
    public class Note : BaseRecord
    {
        public Note()
        {
            Name = "Введите неазвание заметки";
        }
    }
}
