﻿using EventLibrary.Model;
using EventLibrary.ViewModel.Create;
using EventLibrary.ViewModel.Lists;
using EventLibrary.ViewModel.MainPage;
using StkCommon.UI.Wpf.Commands;
using StkCommon.UI.Wpf.ViewModels;
using Unity;
using Unity.Lifetime;

namespace EventDesigner
{
    public class AppRunner : ViewModelBase
    {
        private IUnityContainer _container;

        public ViewModelBase CurrentDataContext { get; set; }

        public DelegateCommand MainButton { get; set; }

        public DelegateCommand EventButton { get; set; }
        public DelegateCommand NoteButton { get; set; }
        public DelegateCommand NotificationButton { get; set; }

        public DelegateCommand AddEventButton { get; set; }
        public DelegateCommand AddNoteButton { get; set; }
        public DelegateCommand AddNotificationButton { get; set; }

        public AppRunner()
        {
            _container = new UnityContainer();

            _container.RegisterType<DesignEventManager>(new ContainerControlledLifetimeManager())
                     .RegisterType<MainPageViewModel>(new ContainerControlledLifetimeManager())
                     
                     .RegisterType<EventsListViewModel>(new ContainerControlledLifetimeManager())
                     .RegisterType<NotesListViewModel>(new ContainerControlledLifetimeManager())
                     .RegisterType<NotificationsListViewModel>(new ContainerControlledLifetimeManager())

                     .RegisterType<CreateEventViewModel>(new ContainerControlledLifetimeManager())
                     .RegisterType<CreateNoteViewModel>(new ContainerControlledLifetimeManager())
                     .RegisterType<CreateNotificationViewModel>(new ContainerControlledLifetimeManager());

            LoadCommands();

            CurrentDataContext = _container.Resolve<MainPageViewModel>();
        }

        private void LoadCommands()
        {
            MainButton = new DelegateCommand(_ => {
                CurrentDataContext = _container.Resolve<MainPageViewModel>();
                OnPropertyChanged();
            });

            EventButton = new DelegateCommand(_ => 
            {
                CurrentDataContext = _container.Resolve<EventsListViewModel>();
                OnPropertyChanged();
            });
            NoteButton = new DelegateCommand(_ =>
            {
                CurrentDataContext = _container.Resolve<NotesListViewModel>();
                OnPropertyChanged();
            });
            NotificationButton = new DelegateCommand(_ =>
            {
                CurrentDataContext = _container.Resolve<NotificationsListViewModel>();
                OnPropertyChanged();
            });

            AddEventButton = new DelegateCommand(_ => 
            {
                CurrentDataContext = _container.Resolve<CreateEventViewModel>();
                ((CreateFormBaseViewModel)CurrentDataContext).Finished += LoadMain;
                OnPropertyChanged();
            });
            AddNoteButton = new DelegateCommand(_ =>
            {
                CurrentDataContext = _container.Resolve<CreateNoteViewModel>();
                ((CreateFormBaseViewModel)CurrentDataContext).Finished += LoadMain;
                OnPropertyChanged();
            });
            AddNotificationButton = new DelegateCommand(_ =>
            {
                CurrentDataContext = _container.Resolve<CreateNotificationViewModel>();
                ((CreateFormBaseViewModel)CurrentDataContext).Finished += LoadMain;
                OnPropertyChanged();
            });
        }

        private void LoadMain()
        {
            CurrentDataContext = _container.Resolve<MainPageViewModel>();
            OnPropertyChanged();
        }
    }
}
