﻿using EventLibrary.Model;
using EventLibrary.Model.Records;
using StkCommon.UI.Wpf.Commands;
using StkCommon.UI.Wpf.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventLibrary.ViewModel.Lists
{
    public class NotesListViewModel : ViewModelBase
    {
        private DesignEventManager _designEventManager;

        public DesignEvent SelectedEvent { get; set; }

        public ObservableCollection<Note> Notes { get; set; }
        
        public DelegateCommand<Note> RemoveNoteCommand =>
            new DelegateCommand<Note>((item) =>
            {
                _designEventManager.RemoveNote(item);
            }, (item) => (item) != null);

        public NotesListViewModel(DesignEventManager designEventManager)
        {
            _designEventManager = designEventManager;

            Notes = _designEventManager.Notes;
        }
    }
}
