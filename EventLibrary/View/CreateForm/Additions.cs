﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace EventLibrary.View.CreateForm
{
    public static class Additions
    {
        public static readonly DependencyProperty TextBoxControllerProperty = 
                DependencyProperty.RegisterAttached( "TextBoxController", 
                                                     typeof(bool), 
                                                     typeof(Additions),
                                                     new FrameworkPropertyMetadata(false, OnTextBoxChanged));

        public static void SetTextBoxController(DependencyObject element, bool value)
        {
            element.SetValue(TextBoxControllerProperty, value);
        }
        public static bool GetTextBoxController(DependencyObject element)
        {
            return (bool)element.GetValue(TextBoxControllerProperty);
        }

        private static void OnTextBoxChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var element = d as TextBox;
            if (element == null)
                throw new ArgumentNullException("d");

            bool t;
            if (element.IsFocused)
                t = true;
        }
        /*
        private static void SelectAll(ITextBlockController sender)
        {
            TextBox element;
            if (!elements.TryGetValue(sender, out element))
                throw new ArgumentException("sender");
            element.Focus();
            element.SelectAll();
        }
        */
    }

    public interface ITextBlockFocusController
    {
        //event SelectAllEventHandler SelectAll;
    }

    //public delegate void SelectAllEventHandler(ITextBlockController sender);
}

