﻿using EventLibrary.Model;
using EventLibrary.Model.Records;
using StkCommon.UI.Wpf.Commands;

namespace EventLibrary.ViewModel.Create
{
    public class CreateNoteViewModel : CreateFormBaseViewModel
    {
        public override DelegateCommand CreateCommand { get; }

        public CreateNoteViewModel(DesignEventManager designEventManger) : base(designEventManger)
        {
            CreateCommand = new DelegateCommand(_ => AddNote());
        }

        private void AddNote()
        {
            if ( NameField == string.Empty )
            {
                NameField = "(без названия)";
            }

            var note = new Note() { Name = NameField,
                                    Сontent = ContentField };
            _designEventManger.AddNote(note);

            NameField = string.Empty;
            ContentField = string.Empty;
            
            OnPropertyChanged();
            СreationFinished();
        }
    }
}
