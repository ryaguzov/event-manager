﻿using EventLibrary.Model;
using StkCommon.UI.Wpf.Commands;
using StkCommon.UI.Wpf.ViewModels;
using System;

namespace EventLibrary.ViewModel.Create
{
    public abstract class CreateFormBaseViewModel : ViewModelBase
    {
        protected DesignEventManager _designEventManger;

        public event Action Finished;
        public abstract DelegateCommand CreateCommand { get; }

        public string NameField { get; set; }
        public string ContentField { get; set; }

        public CreateFormBaseViewModel(DesignEventManager designEventManger)
        {
            _designEventManger = designEventManger;
        }

        protected void СreationFinished()
        {
            Finished.Invoke();
        }
    }
}
