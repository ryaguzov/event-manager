﻿using EventLibrary.Model;
using EventLibrary.Model.Records;
using StkCommon.UI.Wpf.Commands;
using StkCommon.UI.Wpf.ViewModels;
using System.Collections.ObjectModel;

namespace EventLibrary.ViewModel.Lists
{
    public class EventsListViewModel : ViewModelBase
    {
        private DesignEventManager _designEventManager;

        public DesignEvent SelectedEvent { get; set; }

        public ObservableCollection<DesignEvent> Events { get; set; }
        public ObservableCollection<DesignEvent> CurrentEvents { get; set; }        

        public DelegateCommand<DesignEvent> AddToCurrentCommand =>
            new DelegateCommand<DesignEvent>((item) =>
            {
                _designEventManager.AddEvent(item, true);
            }, (item) => (item) != null);
        
        public DelegateCommand<DesignEvent> RemoveEventCommand =>
            new DelegateCommand<DesignEvent>((item) =>
            {                
                _designEventManager.RemoveEvent(item);
            }, (item) => (item) != null);

        public EventsListViewModel(DesignEventManager designEventManager)
        {
            _designEventManager = designEventManager;

            Events = _designEventManager.Events;
            CurrentEvents = _designEventManager.CurrentEvents;
        }     
    }
}
